import type { AWS } from '@serverless/typescript';
import { 
  createUser, loginUser, updateCompanyInfo, updateUserProfile, getAllUser, 
  companyUserCreate, companyUserLogin, companyUserUpdate, companyUserDelete, 
  getAllCompanyUserById, getUsersById, 
  companyUserInvitationDelete, 
  companyUserInvitationUpdate, userPasswordChanged, getAllLanguages, 
  getAllCities, getAllCountry, uploadResource, deleteResource, RetriveResource, 
  getAllRoles, getAllIndustries, getAllTechnicianByCompanyId, getAllServiceProviderByCompanyId, getCompanyUserById,
  verifyEmail,resendVerification, forgotPassword, getTimeZoneList, invitePartner, acceptInvitation, getPartnerListing, getPartnerById, deletePartner, resendInvitation, 
  newUserPartnerInvitations,verifyPassword, //createPermissionKey
} from '@functions/index';

const serverlessConfiguration: AWS = {
  service: 'APIUserManagement',
  frameworkVersion: '3',
  plugins: ['serverless-esbuild','serverless-offline','serverless-dotenv-plugin','serverless-sequelize-migrations'],
  provider: {
    name: 'aws',
    runtime: 'nodejs14.x',
    region:'eu-central-1',
    stage: "${opt:stage,'dev'}",
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    environment: {
      dialect:'mysql',
      DATABASE_NAME:process.env.DATABASE_NAME,
      DATABASE_USERNAME:process.env.DATABASE_USERNAME,
      DATABASE_PASSWORD:process.env.DATABASE_PASSWORD,
      DATABASE_HOST:process.env.DATABASE_HOST,
      DATABASE_PORT:process.env.DATABASE_PORT,
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      NODE_OPTIONS: '--enable-source-maps --stack-trace-limit=1000',
    },
    iamRoleStatements:[
      // {
      //   'Effect': 'Allow',
      //   'Action': ['sqs:SendMessage'],
      //   'Resource': '${self:custom.MailQueue.arn}'
      // },
      {
        "Effect": "Allow",
            "Action": [
                "ses:SendEmail",
                "ses:SendRawEmail"
            ],
            "Resource": "*"
      }
    ]
  },
  // import the function via paths
  functions: { createUser, loginUser, updateCompanyInfo, 
    updateUserProfile, getAllUser,
    companyUserCreate, companyUserLogin, 
    companyUserUpdate, companyUserDelete, getAllCompanyUserById, getUsersById, 
    companyUserInvitationDelete, companyUserInvitationUpdate, userPasswordChanged, 
    getAllLanguages, getAllCities, getAllCountry, RetriveResource, uploadResource, 
    deleteResource, getAllRoles, getAllIndustries, getAllTechnicianByCompanyId, getAllServiceProviderByCompanyId,
    getCompanyUserById, verifyEmail,resendVerification,forgotPassword, getTimeZoneList, invitePartner, acceptInvitation, getPartnerListing, getPartnerById, deletePartner, resendInvitation, 
    newUserPartnerInvitations,verifyPassword
    // createPermissionKey
  },
  package: { individually: true , exclude:['pg-hstore'] },
  custom: {
    esbuild: {
      bundle: true,
      minify: true,
      sourcemap: true,
      exclude: ['aws-sdk'],
      target: 'node14',
      define: { 'require.resolve': undefined },
      platform: 'node',
      concurrency: 10,
    },
  },
  // resources: {
  //   Resources: {
  //     FileBucket:{
  //       Type: 'AWS::S3::Bucket',
  //       Properties: {
  //         BucketName: `${process.env.FILE_UPLOAD_BUCKET_NAME}`,
  //         AccessControl: 'PublicRead'
  //       }
  //     }
  //   }
  // }
};

module.exports = serverlessConfiguration;
