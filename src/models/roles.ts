import { AutoIncrement, Column, DataType, HasOne, Model, PrimaryKey, Table } from "sequelize-typescript";
import { users } from "./users";

export interface rolesI {
    id?: number;
    name?: string;
    key: string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deltedBy?: number;
    roleId: number;
    lang: string;
}

@Table({
    modelName: 'roles',
    tableName: 'roles',
    timestamps: true
})

export class roles extends Model<rolesI>{

    @HasOne((): typeof users => users)
    public users: typeof users;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @Column(DataType.TEXT)
    public name: string;

    @Column(DataType.TEXT)
    public key: string;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

    @Column(DataType.INTEGER)
    public roleId: number;

    @Column(DataType.TEXT)
    public lang: string;
}