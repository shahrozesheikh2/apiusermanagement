import { AutoIncrement, Column, DataType, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface usersRolesI {
    id: number;
    tenentRolesId: number;
    usersId: number;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    createdBy: number;
    updatedBy: number;
    deletedBy: number;
}

@Table({
    modelName: 'usersRoles',
    tableName: 'usersRoles',
    timestamps: true
})

export class usersRoles extends Model<usersRolesI>{

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @Column(DataType.BIGINT)
    public tenentRolesId: number; 

    @Column(DataType.BIGINT)
    public usersId: number; 
    
    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;
    
}