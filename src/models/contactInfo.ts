// import { AutoIncrement, Column, DataType, ForeignKey, HasMany, Model, PrimaryKey, Table } from "sequelize-typescript";
// import { company } from "./company";

// export interface contactInfoI {
//     id?: number;
//     contactPerson:String;
//     contactNumber:JSON;
//     emialAddress?: JSON;
//     companyId:number;
//     primary:number;
//     createdAt?: Date;
//     updatedAt?: Date;
//     deletedAt?: Date;
//     createdBy?: number;
//     updatedBy?: number;
//     deltedBy?: number;
// }

// @Table({
//     modelName: 'contactInfo',
//     tableName: 'contactInfo',
//     timestamps: true
// })

// export class contactInfo extends Model<contactInfoI>{

//     @HasMany((): typeof company => company)
//     public company: typeof company;

//     // @HasMany((): typeof users => users)
//     // public users: typeof users;
    
//     @PrimaryKey
//     @AutoIncrement
//     @Column(DataType.BIGINT)
//     public id: number;
    
//     @Column(DataType.TEXT)
//     public contactPerson: string;

//     @Column(DataType.JSON)
//     public contactNumber: JSON;

//     @Column(DataType.JSON)
//     public emialAddress: JSON;

//     @ForeignKey((): typeof company => company)
//     @Column(DataType.BIGINT)
//     public companyId: number;

//     @Column(DataType.DATE)
//     public createdAt: Date;

//     @Column(DataType.INTEGER)
//     public createdBy: number;

//     @Column(DataType.DATE)
//     public deletedAt: Date;

//     @Column(DataType.INTEGER)
//     public deletedBy: number;

//     @Column(DataType.DATE)
//     public updatedAt: Date;

//     @Column(DataType.INTEGER)
//     public updatedBy: number;
// }