import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, Model, PrimaryKey, Table } from "sequelize-typescript";
import { permissionKey } from "./permissionKey";
import { roles } from "./roles";

export interface rolePermissionI {
    id?: number;
    roleId?: number;
    permissionId?: number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deltedBy?: number;
    route?:string
}

@Table({
    modelName: 'rolePermission',
    tableName: 'rolePermission',
    timestamps: true
})

export class rolePermission extends Model<rolePermissionI>{

    @BelongsTo((): typeof roles => roles)
    public roles: typeof roles;

    @BelongsTo((): typeof permissionKey => permissionKey)
    public permissionKeys: typeof permissionKey;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @ForeignKey((): typeof roles => roles)
    @Column(DataType.BIGINT)
    public roleId: number;

    @ForeignKey((): typeof permissionKey => permissionKey)
    @Column(DataType.BIGINT)
    public permissionId: number;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

}