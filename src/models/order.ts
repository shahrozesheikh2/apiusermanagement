import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, Model, PrimaryKey, Table } from "sequelize-typescript";
import {company} from './company'

export interface orderI {
    id : number;
    fdPartnerId : number;
    customerId : number;
    bookingTypeId?: number;
    subBookingTypeId : number;
    productId:number;
    customTag:string;
    statusId:number;
    companyId:number;
    partialFilled:boolean
    fixFirstId: string;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
    orderNum: number;
}

@Table({
    modelName: 'order',
    tableName: 'order',
    timestamps: true
})

export class order extends Model<orderI>{

    @BelongsTo((): typeof company => company , 'fdPartnerId' )
    public partnerCompany: typeof company;

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id: number;

    @ForeignKey((): typeof company  => company )
    @Column(DataType.INTEGER)
    public fdPartnerId: number;

    @Column(DataType.TINYINT)
    public partialFilled:boolean

    @Column(DataType.TEXT)
    public customTag:string

    @Column(DataType.TEXT)
    public fixFirstId: string

    @Column(DataType.INTEGER)
    public companyId : number;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.INTEGER)
    public orderNum: number;

}