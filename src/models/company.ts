import { AutoIncrement,  BelongsTo,  Column, DataType, HasMany, ForeignKey, HasOne, Model, PrimaryKey, Table } from "sequelize-typescript";
import {  city, users } from ".";
import { country } from "./country";
import { industry } from "./industry";
import {order} from './order'

export interface companyI {
    id?: number;
    logo?: JSON;
    companyName?: string;
    website: string;
    industryId?: number;
    noOfEmployees?: string;
    taxId?: number;
    allExpertise:JSON;
    address?: string;
    addressNotes?: string;
    countryId?: number;
    cityId?: number;
    stateId?: number;
    postalCode?: number;
    isBillingAddress?: boolean;
    billingAddress:string;
    billingAddressNotes:string;
    billingAddressCountryId:number;
    billingAddressCityId:number;
    billingAddressStateId:number;
    billingAddressPostalCode:number;
    bankName:string;
    accountTitle:string;
    iban?: string;
    bic?: string;
    vat?: number;
    legalInformation:JSON;
    payTermsDays:string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deltedBy?: number;
    contactInfo:number;
    // sparePartTax:number;
    // serviceChargesTax:number;
    
    // contact:string;
    // additionalContactNumber:string;
    // email:string;
    // phoneNumber:string
    // payTerms:string;
}

@Table({
    modelName: 'companyInfo',
    tableName: 'company',
    timestamps: true
})

export class company extends Model<companyI>{

    @HasMany((): typeof order => order )
    public order: typeof order;
    
    @HasOne((): typeof users => users)
    public users: typeof users;

    @BelongsTo((): typeof industry => industry)
    public industry: typeof industry;

    @BelongsTo((): typeof country => country)
    public country: typeof country;

  
    @BelongsTo((): typeof city => city)
    public city: typeof city;

    @BelongsTo((): typeof country => country,'billingAddressCountryId')
    public billingAddressCountry: typeof country;

    @BelongsTo((): typeof city => city,'billingAddressCityId')
    public billingAddressCity: typeof city;

    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @Column(DataType.JSON)
    public logo: JSON;

    @Column(DataType.TEXT)
    public companyName: string;

    @Column(DataType.TEXT)
    public website: string;

    @ForeignKey((): typeof industry => industry)
    @Column(DataType.BIGINT)
    public industryId: number;

    @Column(DataType.TEXT)
    public noOfEmployees: string; 

    @Column(DataType.BIGINT)
    public taxId: number;

    @Column(DataType.JSON)
    public allExpertise: JSON;

    @Column(DataType.TEXT)
    public address: string;

    // @Column(DataType.NUMBER)
    // public addressNumber: number;

    // @Column(DataType.TEXT)
    // public addition: string;

    @Column(DataType.TEXT)
    public addressNotes: string;

    @ForeignKey((): typeof country => country)
    @Column(DataType.BIGINT)
    public countryId: number;

    @ForeignKey((): typeof city => city)
    @Column(DataType.BIGINT)
    public cityId: number;

    // @ForeignKey((): typeof city => city)
    @Column(DataType.BIGINT)
    public stateId: number;

    @Column(DataType.BIGINT)
    public postalCode: number;

    @Column(DataType.TINYINT)
    public isBillingAddress: boolean;

    @Column(DataType.STRING)
    public billingAddress: string;

    @Column(DataType.STRING)
    public billingAddressNotes: string;

    @ForeignKey((): typeof country => country)
    @Column(DataType.INTEGER)
    public billingAddressCountryId: number;
    
    @ForeignKey((): typeof city => city)
    @Column(DataType.INTEGER)
    public billingAddressCityId: number;

    @ForeignKey((): typeof city => city)
    @Column(DataType.INTEGER)
    public billingAddressStateId: number;

    @Column(DataType.INTEGER)
    public billingAddressPostalCode: number;

    @Column(DataType.TEXT)
    public bankName: string;

    @Column(DataType.TEXT)
    public accountTitle: string;

    @Column(DataType.TEXT)
    public iban: string;

    @Column(DataType.TEXT)
    public bic: string;

    @Column(DataType.BIGINT)
    public vat: number;

    @Column(DataType.JSON)
    public legalInformation: JSON;

    @Column(DataType.TEXT)
    public payTermsDays: string;

    @Column(DataType.JSON)
    public contactInfo: JSON;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

    // @Column(DataType.INTEGER)
    // public sparePartTax: number;
    
    // @Column(DataType.INTEGER)
    // public serviceChargesTax: number;
    
   

    // @Column(DataType.TEXT)
    // public contact: string;

    // @Column(DataType.TEXT)
    // public additionalContactNumber: string;

    // @Column(DataType.TEXT)
    // public email: string;

    // @Column(DataType.TEXT)
    // public phoneNumber: string;

    // @Column(DataType.TEXT)
    // public additionalPhoneNumber: string;

  

    // @Column(DataType.TEXT)
    // public payTerms: string;

  

}
