import { AutoIncrement, Column, DataType, HasOne, Model, PrimaryKey, Table } from "sequelize-typescript";
import { users } from ".";

export interface tenentI {
    id: number;
    email: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    createdBy: number;
    updatedBy: number;
    deletedBy: number;
}

@Table({
    modelName: 'tenent',
    tableName: 'tenent',
    timestamps: true
})

export class tenent extends Model<tenentI>{

    // @HasOne((): typeof companyInfo => companyInfo)
    // public companyInfo: typeof companyInfo;

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @Column(DataType.TEXT)
    public email: string; 
    
    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;
    
}