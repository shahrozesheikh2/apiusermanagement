import { AutoIncrement, Column, DataType, HasMany, Model, PrimaryKey, Table } from "sequelize-typescript";
import { rolePermission } from "./rolePermission";

export interface permissionKeyI {
    id?: number;
    key?: string;
    route?: string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deltedBy?: number;
}

@Table({
    modelName: 'permissionKey',
    tableName: 'permissionKey',
    timestamps: true
})

export class permissionKey extends Model<permissionKeyI>{

    @HasMany((): typeof rolePermission => rolePermission)
    public rolePermission: typeof rolePermission;
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @Column(DataType.TEXT)
    public key: string;

    @Column(DataType.TEXT)
    public route: string;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

}