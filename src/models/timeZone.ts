import { Table, PrimaryKey, AutoIncrement, Column, DataType, Model } from "sequelize-typescript";

export interface timeZoneI {
  id: number,
  timeZoneName: string,
  timeZone: number
}

@Table({
  modelName: 'timeZone',
  tableName: 'timeZone',
  timestamps: false
})

export class timeZone extends Model<timeZoneI>{
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @Column(DataType.TEXT)
    public timeZoneName: string

    @Column(DataType.BIGINT)
    public timeZone: number
}