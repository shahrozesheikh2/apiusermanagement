import { AutoIncrement, Column, DataType, HasOne, Model, PrimaryKey, Table } from "sequelize-typescript";
import { company } from "./company";

export interface industryI {
    id?: number;
    name?: string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deltedBy?: number;
    industryId?: number;
    lang?: string;

}

@Table({
    modelName: 'industry',
    tableName: 'industry',
    timestamps: true
})

export class industry extends Model<industryI>{
    
    @HasOne((): typeof company => company)
    public company: typeof company;

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @Column(DataType.TEXT)
    public name: string;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

    @Column(DataType.TEXT)
    public lang: string;

    @Column(DataType.INTEGER)
    public industryId: string;
}