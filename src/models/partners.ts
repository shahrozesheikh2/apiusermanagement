import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, Model, PrimaryKey, Table } from "sequelize-typescript";
import {  company, users } from ".";


export interface partnersI {
  partnershipId: number;
  userId1: number;
  userId2: number;
  companyId1: number;
  companyId2: number;
  accepted: boolean;
  genratedLink: string;
  invitedEmails: string;
  invitedEmailsDate: Date;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  createdBy?: Date;
  updatedBy?: Date;
  deletedBy?: Date;
}

@Table({
  modelName: 'partners',
  tableName: 'partners',
  timestamps: true
})

export class partners extends Model<partnersI>{

  // @BelongsTo((): typeof company => company)
  // public company: typeof company; 

  @BelongsTo((): typeof company => company, 'companyId2')
  public company2: typeof company; 

  @BelongsTo((): typeof company => company, 'companyId1')
  public company1: typeof company; 

  @BelongsTo((): typeof users => users, 'userId1')
  public user1: typeof users; 

  @BelongsTo((): typeof users => users, 'userId2')
  public user2: typeof users; 

  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  public partnershipId: Number;

  @ForeignKey((): typeof users => users)
  @Column(DataType.BIGINT)
  public userId1: Number;

  @ForeignKey((): typeof users => users)
  @Column(DataType.BIGINT)
  public userId2: Number;

  @ForeignKey((): typeof company => company)
  @Column(DataType.BIGINT)
  public companyId1: number;

  @ForeignKey((): typeof company => company)
  @Column(DataType.BIGINT)
  public companyId2: Number;

  @Column(DataType.TINYINT)
  public accepted: Boolean;

  @Column(DataType.TEXT)
  public genratedLink: string;

  @Column(DataType.DATE)
  public createdAt: Date;

  @Column(DataType.INTEGER)
  public createdBy: number;

  @Column(DataType.DATE)
  public deletedAt: Date;

  @Column(DataType.INTEGER)
  public deletedBy: number;

  @Column(DataType.DATE)
  public updatedAt: Date;

  @Column(DataType.INTEGER)
  public updatedBy: number;

  @Column(DataType.TEXT)
  public invitedEmails: string;

  @Column(DataType.DATE)
  public invitedEmailsDate: Date;
}