import { ANY } from '../shared/common';
import { 
    // tenent,
    users,
    company,
    // companyUser,
    // usersRoles,
    city,
    country,
    languages,
    roles,
    industry,
    timeZone,
    partners,
    order,
    permissionKey,
    rolePermission,
    // contactInfo
    // companyUsersRoles
} from '.';

// export * from './tenent';
export * from './users';
export * from './company';
// export * from './companyUser';
// export * from './usersRoles';
export * from './city';
export * from './country';
export * from './languages'
export * from './roles'
export * from './industry'
// export * from './companyUsersRoles'
export * from './timeZone'
export * from './partners'
export * from './order'
export * from './permissionKey'
export * from './rolePermission'
// export * from './contactInfo'




type ModelType = ANY;

export const models: ModelType = [
     users, company, city, country, languages,
      roles, industry, timeZone, partners,
      permissionKey,rolePermission,order
    //   contactInfo
]