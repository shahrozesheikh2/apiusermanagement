import AwsConfig from './AwsConfig'


function signUp(email: any, password: any, agent = 'none') {
  return new Promise((resolve) => {
    AwsConfig.initAWS ();
    AwsConfig.setCognitoAttributeList(email,agent);
    AwsConfig.getUserPool().signUp(email, password, AwsConfig.getCognitoAttributeList(), null, function(err: any, result: { user: { username: any; client: { userAgent: any; }; }; userConfirmed: any; }){
      if (err) {
        return resolve({ statusCode: 422, response: err });
      }
      const response = {
        username: result.user.username,
        userConfirmed: result.userConfirmed,
        userAgent: result.user.client.userAgent,
      }
        return resolve({ statusCode: 201, response: response });
      })
    })
}

function verify(email: any, code: any) {
  return new Promise((resolve) => {
    AwsConfig.getCognitoUser(email).confirmRegistration(code, true, (err: any, result: any) => {
      if (err) {
        return resolve({ statusCode: 422, response: err });
      }
      return resolve({ statusCode: 400, response: result });
    });
  });
}

function signIn(email: any, password: any) {
  return new Promise((resolve) => {
    AwsConfig.getCognitoUser(email).authenticateUser(AwsConfig.getAuthDetails(email, password), {
      onSuccess: (result: { getAccessToken: () => { (): any; new(): any; getJwtToken: { (): any; new(): any; }; }; getIdToken: () => { (): any; new(): any; getJwtToken: { (): any; new(): any; }; }; getRefreshToken: () => { (): any; new(): any; getToken: { (): any; new(): any; }; }; }) => {
        const token = {
          accessToken: result.getAccessToken().getJwtToken(),
          idToken: result.getIdToken().getJwtToken(),
          refreshToken: result.getRefreshToken().getToken(),
        }  
        return resolve({ statusCode: 200, response: AwsConfig.decodeJWTToken(token) });
      },
      
      onFailure: (err: { message: any; }) => {
        return resolve({ statusCode: 400, response: err.message || JSON.stringify(err)});
      },
    });
  });
}

export default {
    signUp,
    verify,
    signIn
}