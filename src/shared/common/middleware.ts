
import { formatJSONResponse } from '@libs/api-gateway';
import { permissionKey, roles } from "src/models";
import { rolePermission } from 'src/models/rolePermission';
// import jwt from 'jsonwebtoken'
const customMiddleware = () => {
  // const options = { ...defaults, ...opts }

  const customMiddlewareBefore = async (request) => {
    try {
      console.log("dasdasdds", request.event)
      // let authorization = request.event.headers['Authorization'].split(' ');
      // console.log("authorizationqqq", authorization)

      //   var decoded = await jwt.verify(authorization[1], process.env.secret);
      //   console.log("decoded", decoded)

      const splittedUrl = request.event.path.split("/");
      console.log("splittedUrl", splittedUrl)
      let roleKey: any = request.event.headers.roleKey
      console.log("roleKey", roleKey)
      if(!roleKey){
        roleKey = 'super_admin'
        
        // throw Error ('Role Key Missing')
      }
      const role = await roles.findOne({ where: { deletedAt: null, key: roleKey } })
      
      let rolePermissions = await rolePermission.findAll({
        where: { roleId: role.id, deletedAt: null },
        include: {
          as: 'permissionKeys',
          model: permissionKey
        }
      })
      rolePermissions = JSON.parse(JSON.stringify(rolePermissions))
      var access = false;
      for (let single of rolePermissions) {
        // const permissionKeys = single.toJSON()

        if (single.permissionKeys?.route === splittedUrl[1]) {
          access = true;
          break;
        }
      }
      access = true;
      if (!access) {
        // throw new Error("No permissions");
        return formatJSONResponse({
          statusCode: 403,
          message: `No permissions`,

        })
      }


    } catch (error) {
      console.error(error);
      // await sequelize.connectionManager.close();
      return formatJSONResponse({
        statusCode: 403,
        message: error.message
      });
    }
  }

  return {
    before: customMiddlewareBefore,
    //   after: customMiddlewareAfter,
    //   onError: customMiddlewareOnError
  }
}

export default customMiddleware