import Crypto from 'crypto'

// generate random number 
export const randomString = (size = 50) => {  
    return Crypto
      .randomBytes(size)
      .toString('base64')
      .slice(0, size)
}

// genrate only alphabet and number
export const makeid = (length: number) => {
    let result = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * 
        charactersLength
        ));
    }
   return result;
}

// export const params = () => {

// }


// const params = JSON.stringify({
//     subject: 'hello from fixfirst',
//     body: `Hello ${user.email} you have successfull register your account please click the link to complete the registration http://app.fixfirst.io/auth/sign-up/step-4?id=${user.id}&genratedLink=${user.genratedLink}&rolesId=${user.rolesId}`,
//     recipient: user.email
//   })




