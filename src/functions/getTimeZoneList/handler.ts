import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from "@libs/api-gateway"
import { sequelize } from 'src/config/database';
import { middyfy } from '@libs/lambda';
import schema from './schema';
import { timeZone } from "src/models";
// import * as typings from '../../shared/common';

const getTimeZoneList:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async()=>{
  
  try{ 
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
  
    const response = await timeZone.findAll();

    await sequelize.connectionManager.close();
  
    return formatJSONResponse({
      statuscode: 200,
      response
  })
}
 catch(error) {
  console.error(error);
  await sequelize.connectionManager.close();

  return formatJSONResponse({
    statusCode: 403,
    message: error.message
  }); 
 }

}

export const main = middyfy(getTimeZoneList)