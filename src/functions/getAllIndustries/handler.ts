import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { industry } from '../../models/index';
import schema from './schema';
import * as typings from '../../shared/common';
// const { I18n } = require('i18n');
// const path = require('path');

// const i18n = new I18n({
//   locales: ['en', 'de'],
//   defaultLocale: 'en',
//    directory: path.join('./', 'locales')
// });


const getAllIndustries:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    var lang = event.headers['accept-language']

    
    console.log(event.headers);

     const industries : typings.ANY = await industry.findAll({where:{ lang },
      attributes: {exclude: ['createdBy', 'updatedBy', 'deletedAt', 'deletedBy', 'createdAt', 'updatedAt']}
    })
    await sequelize.connectionManager.close();
    // i18n.setLocale(event.headers)
    // console.log(i18n.__('Hello')); // 'Hello'

     return formatJSONResponse({
      statusCode: 200,
      message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,
       industries
     });
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(getAllIndustries);