import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { city, country, languages, roles, users } from '../../models/index';
import schema, { getCompanyUserByIdSchema } from './schema';
import * as typings from '../../shared/common';


const getCompanyUserById:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const data :typings.ANY   = await getCompanyUserByIdSchema.validateAsync(event.body)
    const{
        id
    } = data
    const response : typings.ANY = await users.findOne({ where:{ id, adminTypeId: null }, 
      attributes: {exclude: ['createdBy', 'updatedBy', 'deletedAt', 'deletedBy']},
      include: [
        {
          as: 'roles',
          model: roles,
          attributes: ['id', 'name']
        },
        // {
        //   as: 'company',
        //   model: company,
        //   attributes: {exclude: ['createdBy', 'updatedBy', 'deletedAt', 'deletedBy']},
        // },
        // {
        //   as: 'language',
        //   model: languages,
        //   attributes: ['id', 'name']
        // },
        {
          as: 'countries',
          model: country,
          attributes: ['id', 'name']
        },
        {
          as: 'cities',
          model: city,
          attributes: ['id', 'name']
        }
      ]
    })

      if(!response){
        throw Error("Benutzer existiert nicht mit aktueller Id")
        // User Not Exists Against Current Id
      }
      await sequelize.connectionManager.close();

    return formatJSONResponse({
    statusCode: 200,
    message: `Erfolgreich erhalten`,
      // message: `Successfully Recieved`,
      response
    });
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(getCompanyUserById);