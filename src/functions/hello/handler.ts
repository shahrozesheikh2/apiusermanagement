import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { tenent } from '../../models/index';
import { sequelize } from '../../config/database';
import schema from './schema';

const hello: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {

  try{
    console.log('HIT 1');
    sequelize.authenticate()
    console.log('HIT 2');
    const check = await tenent.findAll()
    console.log('connct',check)
    return formatJSONResponse({
      message: `Hello ${event.body}, welcomrprpe to the exciting Serverless world!`,
      check
    });
    
  } catch(error) {
    console.error(error);
    
  }
};

export const main = middyfy(hello);
