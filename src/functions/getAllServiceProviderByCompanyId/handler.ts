import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { users, roles } from '../../models/index';
import schema, { getServiceProviderSchema } from './schema';
import * as typings from '../../shared/common';
import { Op } from 'sequelize';


const getAllServiceProviderByCompanyId:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const data :typings.ANY   = await getServiceProviderSchema.validateAsync(event.body)
    const{
      companyId,
      preferedLanguage
    } = data

    let  clause: { [key: string]: typings.ANY }  = {}
    
    if(preferedLanguage && preferedLanguage.length){
      clause.languages = { [Op.in]: preferedLanguage}
    }
    const response : typings.ANY = await users.findAll({where:{ 
        companyId, ...clause, deletedAt: null, adminTypeId: null
        // languages: {
        //   [Op.in]: preferedLanguage
        // } 
      }, 
      attributes: [
        'id', 'companyId', 'firstName', 
        'lastName', 'email', 'languages', 'createdAt', 'updatedAt'
      ],
      include:{
        as: 'roles',
        model: roles,
        where: {key: 'service_provider'},
        attributes:['name']
      }
    })

    
    if(!response.length){
    await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 403,
        message: `Benutzer existiert nicht mit aktueller ID`,
        // message: `User Not Exists Against Current Id`,
      });
    }

    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 200,
      message: `Erfolgreich erhalten`,
      // message: `Successfully Recieved`,
      response
    });

  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(getAllServiceProviderByCompanyId);