import * as Joi from 'joi'

export default {
    properties: {
    }
} as const;


export const getServiceProviderSchema =  Joi.object({
    companyId: Joi.number().integer().required(),
    // filters: Joi.object({
    //     preferedLanguage: Joi.number().integer(),
    // }),
    preferedLanguage: Joi.array().items(Joi.number().integer())
})