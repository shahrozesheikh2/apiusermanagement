import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;

export const companyUserRegisterScheme = Joi.object({ 
  permissions: Joi.array().items({
    key:Joi.string().required(),
    route:Joi.string().required(),
  }).required(),  
})