import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { permissionKey, rolePermission } from '../../models/index';
import { sequelize } from '../../config/database';
import schema, {companyUserRegisterScheme} from './schema';
import * as typings from '../../shared/common';


const createPermissionKey: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
  
      const obj: typings.ANY = await companyUserRegisterScheme.validateAsync(event.body)

      let permission:any = obj.permissions;

      console.log("permission",permission)

      let arry = []

      for (let i=1;i<=123;i++){
        arry.push({
          roleId:1,
          permissionId:i
        })
      }
      
      // const permissionsKeys = await rolePermission.bulkCreate(permission)
     
      await sequelize.connectionManager.close();
      
      return formatJSONResponse({
        statusCode: 200,
        message: 'created Successfully',
        arry
      });
    
  } 
  catch(error) {
    await sequelize.connectionManager.close();
    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
}

export const main = middyfy(createPermissionKey);
