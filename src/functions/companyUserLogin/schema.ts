import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;

export const companyUserLoginScheme = Joi.object({ 
    email: Joi.string().email({ tlds: { allow: ['com', 'net'] } }).required(),
    password:Joi.string().required(), 
})