import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { users } from '../../models/index';
import { sequelize } from '../../config/database';


const verifyEmail = async (event) => {
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    
    try{

        const params = event.queryStringParameters

        const findToken = await users.findOne({ where:  { genratedLink: params.genratedLink  } })
        
        if(!findToken){
            await sequelize.connectionManager.close();

            return formatJSONResponse({
              message: `entered wrong token`,
            })
        }

        if(params.userStatusId){
          await users.update({ emailVerified: true, userStatusId: 2, updatedAt: new Date }, {
            where: {  id: params.id, adminTypeId: null  }
          })  
        }

        if(!params.userStatusId){
          await users.update({ emailVerified: true, updatedAt: new Date }, {
            where: {  id: params.id, adminTypeId: 1 }
          })
        }

        // await users.update({ emailVerified: true, updatedAt: new Date }, {
        //       where: {  id: params.id }
        // })
        
        await sequelize.connectionManager.close();
      
      return formatJSONResponse({
        statusCode: 200,
        message: `Benutzer erfolgreich verifiziert`,
        // User Successfully verified
      })
    } 
    catch (error: any) {
      await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 403,
        message: error.message
      })
    }
  } 
  catch(error) {
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
};

export const main = middyfy(verifyEmail);
