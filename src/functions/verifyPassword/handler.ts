import { formatJSONResponse } from '@libs/api-gateway';
import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { users } from '../../models/index';
import { sequelize } from '../../config/database';
import schema, {verifySchema} from './schema';
import bcrypt from "bcryptjs";


const verifyPassword: ValidatedEventAPIGatewayProxyEvent<typeof schema>  = async (event) => {
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    
    const data = await verifySchema.validateAsync(event.body)

      const{
        email,
        password
      }= data

      const userData = await users.findOne({where:{email, deletedAt:null}})

      if(!userData){
        await sequelize.connectionManager.close();

        return formatJSONResponse({
          statusCode: 404,
          // message: "Email wurde nicht gefunden"
          message: "Email not found"
        }) 
      }

      const checkPassword = bcrypt.compareSync(password, userData.password)

      if(!checkPassword){
        await sequelize.connectionManager.close();

        throw Error ('Invalid Password')

        // return formatJSONResponse({
        //   statusCode: 404,
        //   // message: "Etwas ist schief gelaufen, bitte überprüfen Sie Ihre E-Mail und Ihr Passwort"
        //   message: "Invalid Password"
        // })
      }
   
      await sequelize.connectionManager.close();
      
      return formatJSONResponse({
        statusCode: 200,
        // message: `Benutzer erfolgreich verifiziert`,
        message:  'Successfully verified'
      })
    
  
  } 
  catch(error) {
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
};

export const main = middyfy(verifyPassword);
