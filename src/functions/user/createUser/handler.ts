import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import {  users, usersI, company, companyI, roles } from '../../../models/index';
import { sequelize } from '../../../config/database';
import schema, {userRegisterSchema} from './schema';
import * as typings from '../../../shared/common';
// import Cognito from './../../../shared/common/cognito';
import bcrypt from "bcryptjs";
import { makeid } from '../../../shared/common/common';
// import AWS from 'aws-sdk'
// import Axios from 'axios'
import fetch from 'node-fetch'

const createUser: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
  try{
    
    sequelize.connectionManager.initPools();
   
    // restore `getConnection()` if it has been overwritten by `close()`

    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {

      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message = 'User Created Successfully'
    var message1 = `Email already exist`
    if(lang =='en')
    {
      lang ='';
    }
    if(lang =='de')
    {
       message = 'Benutzerkonto erfolgreich erstellt'
       message1 ="E-Mail existiert bereits"
    }
    const transaction = await sequelize.transaction()
    // const ses = new AWS.SES({ region: 'eu-central-1' }) 
    
    try{
      const registerScheme: typings.ANY = await userRegisterSchema.validateAsync(event.body)

      const{
        usersInfo,
        companyDetail
      }= registerScheme

      const {
        firstName,
        lastName,
        password,
        // rolesId,
        email
      } = usersInfo


      const duplicateEmail = await users.findOne({ where:  { email, deletedAt: null } }) 

      if(duplicateEmail){
        // await sequelize.connectionManager.close();
        return formatJSONResponse({
          message:message1 ,
        })
      }

      // const params ={
      //   Source: 'admin@fixfirst.io',
      //   Desination: {
      //     ToAddresses: [email],
      //   },
      //   Message: {
      //     Body:{
      //       Text:{
      //         Data: 'Hello from fixfirst'
      //       }
      //     },
      //     Subject: {
      //       Data: 'test mail'
      //     }
      //   }
      // }
      
      // const {
      //   noOfEmployees,
      //   ...otherAttributes
      // } = companyDetail

      // const{
      //   min:minNoOfEmp,
      //   max:maxNoOfEmp
      // }=noOfEmployees

      const companyResponse = await company.create(companyDetail, { transaction })

      const companyInfoObj: any = {
        companyId: companyResponse.id,
        // usersRolesId :usersRolesResponse.id,
        // minNoOfEmp,
        // maxNoOfEmp,
        // ...otherAttributes,
      }

      // const companyInfoResponse: companyI = await company.create(companyInfoObj, { transaction })

      // const tenentResponse: usersI = await users.create(usersInfo ,{transaction})

      let superAdminRole = await roles.findOne({where:{
        key:'super_admin',
        deletedAt:null
      }})

      console.log("superAdminRole",superAdminRole)

      const genratedLink = `${makeid(150)}`
      const usersObj: any = {
        companyId: companyResponse.id,
        firstName,
        lastName,
        rolesId:superAdminRole.id,
        email,
        emailVerified:0,
        adminTypeId:1,
        genratedLink,
        active:true
      }
      const passwordHash =await bcrypt.hash(password, 10);
      usersObj.password = passwordHash
      const usersResponse: usersI = await users.create(usersObj, {transaction})
      
      // const usersRoleObj: any = {
      //   usersId :usersResponse.id,
      //   rolesId: rolesId
      // }
      // const usersRolesResponse: usersRolesI = await usersRoles.create(usersRoleObj, {transaction})
      
     var params = {
        subject: 'Fixfirst confirmation',
        body: `Hello ${firstName} ${lastName},
               
              Your confirmation link is below — click on the given link to verify your account and get signed in.
              
              https://dev.app.fixfirst.io/auth/sign-up/step-4?id=${usersResponse.id}&genratedLink=${usersResponse.genratedLink}&rolesId=${usersResponse.rolesId}
              
              If you didn’t request this email, there’s nothing to worry about — you can safely ignore it. 
              
              Thanks! 
              Team FixFirst`,
        recipient: email
      }
     
      if(lang =='de')
      {
         params = {
          subject: 'FixFirst E-Mail Bestätigung',
          body:`Bestätige deine Email-Adresse 
                Hallo ${firstName} ${lastName}, 
                bitte klicke unten auf den angegebenen Link, um dein Konto zu verifizieren und dich anschließend anzumelden.
                https://dev.app.fixfirst.io/${lang}/auth/sign-up/step-4?id=${usersResponse.id}&genratedLink=${usersResponse.genratedLink}&rolesId=${usersResponse.rolesId}
                Wenn du diese E-Mail nicht angefordert hast, brauchst du dir keine Sorgen zu machen - Du kannst sie einfach ignorieren.
                              
                Vielen Dank! 
                Dein FixFirst-Team`,
          recipient: email
        }
      }
    



      const result = {
        companyResponse,
        usersResponse,
        // usersRolesResponse,
        // companyInfoResponse
      }

      // await Cognito.signUp(usersInfo.email,usersInfo.password)

      // await Cognito.verify(usersInfo.email,'654769')
      
      const emailServiceResponse = await fetch(`${process.env.SEND_EMAIL_SES}/sendMail`, {
        method: 'POST',
        body: JSON.stringify(params),
        headers: { 'Content-Type': 'application/json' }
      })
      .then(res => res.json())
      .then(json => json)
      .catch(err => err);

      await transaction.commit()
      
      // await Axios.post(`${process.env.SEND_EMAIL_SES}/sendMail`, params);
      await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 200,
        message: message,
        // message: `User Created Successfully`,
        result,
        emailServiceResponse
      })
    } 
    catch (error: any) {
      if (transaction) {transaction.rollback(); }
      await sequelize.connectionManager.close();
      return formatJSONResponse({
        statusCode: 403,
        message: error.message
       
      })
    }
  } 
  catch(error) {
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
};

export const main = middyfy(createUser);