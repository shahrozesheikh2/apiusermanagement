import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;
  
export const userRegisterSchema = Joi.object({   
  usersInfo:Joi.object({
    firstName:Joi.string().required(),
    lastName:Joi.string().required(),
    email:Joi.string().email().required(),  
    password: Joi.string().required().min(8),
    // rolesId: Joi.number().integer().required(),
  }).required(),
  companyDetail:Joi.object({
    companyName: Joi.string().required(),
    // noOfEmployees: Joi.object({
    //   min:Joi.number().integer().required(),
    //   max:Joi.number().integer().required(),
    // }).required(), 
    noOfEmployees: Joi.string().required(),
    industryId: Joi.number().integer().required(),
  }).required()
})

// address: Joi.string().required(),
    // addressNumber: Joi.number().integer().required(),
    // countryId: Joi.number().integer().required(),
    // postalCode: Joi.number().integer().required(),
    // cityId: Joi.number().integer().required(),
  //   logo: Joi.object({
  //     awsKey: Joi.string().required(),
  //     awsUrl: Joi.string().uri().required()
  // }).max(2)

 
    