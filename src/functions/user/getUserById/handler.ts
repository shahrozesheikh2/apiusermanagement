import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { users, company, industry, country, city, roles } from '../../../models/index';
import schema, { getUserByIdSchema } from './schema';
import * as typings from '../../../shared/common';
import customMiddleware from '../../../shared/common/middleware'

const getUsersById:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const data :typings.ANY   = await getUserByIdSchema.validateAsync(event.body)
    const{
        id
    } = data
    let response : typings.ANY = await users.findOne({ where:{ id}, 
      attributes: {exclude: ['createdBy', 'updatedBy', 'deletedAt', 'deletedBy']},
      include:[
        {
          as: 'roles',
          model: roles,
          attributes: ['id', 'name']
        },
        {
          as: 'company',
          model: company,
          include: [
            {
              as: 'industry',
              model: industry,
              attributes: ['id', 'name']
            },
            {
              as: 'country',
              model: country,
              attributes: ['id', 'name']
            },
            {
              as: 'city',
              model: city,
              attributes: ['id', 'name']
            },
            {
              as: 'billingAddressCountry',
              model: country,
              attributes: ['id', 'name']
            },
            {
              as: 'billingAddressCity',
              model: city,
              attributes: ['id', 'name']
            },
          ],
          attributes: {exclude: ['createdBy', 'updatedBy', 'deletedAt', 'deletedBy']},
        }
      ]
    })

  //   let billingAddressCountry, billingAddressCity

  //   if(response.company.billingAddressCountryId && response.company.billingAddressCityId){
  //     console.log("response.company.billingAddressCountryId",response.company.billingAddressCountryId)
  //     console.log("response.company.billingAddressCityId",response.company.billingAddressCityId)
  //     billingAddressCountry = await country.findOne({attributes:['id','name'],where:{id: response.company.billingAddressCountryId}})
  //     billingAddressCity = await city.findOne({attributes:['id','name'], where: {id: response.company.billingAddressCityId} })
  //     // response.company.billingAddressCity = billingAddressCity
  //     // console.log("response.company.billingAddressCountryId",billingAddressCity)
  //  }

      if(!response){
        throw Error("Benutzer existiert nicht mit aktueller ID")
        // User Not Exists Against Current Id
      }
    
    // response = { ...response, }

    // response.company = {...response.company,billingAddressCountry,billingAddressCity}
    
    await sequelize.connectionManager.close();
    return formatJSONResponse({
    statusCode: 200,
    message: `Erfolgreich erhalten`,
      // message: `Successfully Recieved`,
      response
    });
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();
    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
// export const main = middy(getUsersById).use(domainValidationMiddleWare());
export const main = middyfy(getUsersById) //.use(customMiddleware())
