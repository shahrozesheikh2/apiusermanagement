import * as Joi from 'joi'

export default {
    type: "object",
    properties: {
    }
} as const;
  
export const userUpdateScheme = Joi.object({    
  companyId:Joi.number().integer().required(), 
  usersInfo:Joi.object({
    firstName:Joi.string(),
    lastName:Joi.string(),
    organizationName: Joi.string(),
    website: Joi.string(),
    brand: Joi.string(),
    mobileNumberCode: Joi.string(),
    mobileNumber: Joi.string(),
    mobileIsWhatsapp: Joi.boolean(),
    additionalNumberCode: Joi.string(),
    additionalNumber: Joi.string(),
    additionalIsWhatsapp: Joi.boolean(),
    // taxId: Joi.string()
  }).required()
})

 
    