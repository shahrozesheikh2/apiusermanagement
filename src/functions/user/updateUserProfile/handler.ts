import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { users } from '../../../models/index';
import { sequelize } from '../../../config/database';
import schema, { userUpdateScheme } from './schema';
import * as typings from '../../../shared/common';
// import bcrypt from "bcryptjs";

const updateUserProfile: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {

  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message = 'Successfully updated'
    if(lang =='de')
    {
       message = `Erfolgreich geupdated`
    }
      const updateScheme: typings.ANY = await userUpdateScheme.validateAsync(event.body) 
      const{
        companyId,
        usersInfo
      }= updateScheme

      const Data = await users.findOne({where:{ companyId, adminTypeId: 1 }})

      if(!Data){
        await sequelize.connectionManager.close();

        return formatJSONResponse({
          statusCode: 403,
          message: `invalid ID`,
        });
      }

      // const hash = await bcrypt.hash(usersInfo.password, 10);
      // usersInfo.password = hash;

      const response: any = await users.update({ ...usersInfo, updatedAt: new Date }, {
        where: { companyId, adminTypeId: 1 }
      })
      await sequelize.connectionManager.close();
      
      return formatJSONResponse({
        statusCode: 200,
        message: message,
        response  
      });
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
}

export const main = middyfy(updateUserProfile);
