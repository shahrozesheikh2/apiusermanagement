import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { city, company, country, languages, roles, users} from '../../../models/index';
import { sequelize } from '../../../config/database';
import schema, {userLoginScheme} from './schema';
import * as typings from '../../../shared/common';
import bcrypt from "bcryptjs";
var axios = require("axios").default;
import jwt from 'jsonwebtoken'

const loginUser: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

      const loginScheme: typings.ANY = await userLoginScheme.validateAsync(event.body)
      const{
        email,
        password
      }= loginScheme

      let data = await users.findOne({
        where:{ email, deletedAt: null, emailVerified: true ,active:true },
      include:[
        {
          as: 'roles',
          model: roles,
          attributes: ['id', 'name','key']
        },
        // {
        //   as: 'language',
        //   model: languages,
        //   attributes: ['id', 'name']
        // },
        {
          as: 'countries',
          model: country,
          attributes: ['id', 'name']
        },
        {
          as: 'cities',
          model: city,
          attributes: ['id', 'name']
        },{
          as: 'company',
          model: company,
          attributes:  ['id', 'companyName','logo','noOfEmployees']
        }
    ]
        // attributes:  ['email', 'password']
      })

      if(!data){
        await sequelize.connectionManager.close();

        return formatJSONResponse({
          statusCode: 404,
          // message: "Email wurde nicht gefunden"
          message: "Email not found"
        }) 
      }

    const checkPassword = bcrypt.compareSync(password, data.password)

      if(!checkPassword){
        await sequelize.connectionManager.close();

        return formatJSONResponse({
          statusCode: 404,
          message: "Etwas ist schief gelaufen, bitte überprüfen Sie Ihre E-Mail und Ihr Passwort"
          // message: "something went wrong please check your email and password"
        })
      }

      var options = {
        method: 'POST',
        url: 'https://auth-service-lambda.eu.auth0.com/oauth/token',
        headers: {'content-type': 'application/json'},
        data: {
          grant_type: 'password',
          client_id: 'NDBb0FDNa98TsgfLv2Q7KOAlqBoBnAnM',
          username: email,
          password: password,
          connection: "CONNECTION",
          scope: 'openid'
        }
      };
      
      
      const token = await axios.request(options)
        .then( response => { return response.data })
        .catch( error => { return error } )

      // console.log("process.env.secret",process.env.secret)
      //   let token = jwt.sign({
      //     id: data.id,
      //     roleId: data.rolesId,
      // }, process.env.secret, { expiresIn: '12D' })

      const response = {
        // token: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkxqZzFtTVNwSFJmcWx4ZE9WSXNOaiJ9.eyJuaWNrbmFtZSI6InNoYXlhbiIsIm5hbWUiOiJzaGF5YW5AZml4Zmlyc3QuaW8iLCJwaWN0dXJlIjoiaHR0cHM6Ly9zLmdyYXZhdGFyLmNvbS9hdmF0YXIvYTIxMWQ3NzdlNDdmOGFjZjBjMmEwNTJiODUwMWRlYjM_cz00ODAmcj1wZyZkPWh0dHBzJTNBJTJGJTJGY2RuLmF1dGgwLmNvbSUyRmF2YXRhcnMlMkZzaC5wbmciLCJ1cGRhdGVkX2F0IjoiMjAyMi0wNC0yMVQwNjo0MDoyMi43NTJaIiwiZW1haWwiOiJzaGF5YW5AZml4Zmlyc3QuaW8iLCJpc3MiOiJodHRwczovL2F1dGgtc2VydmljZS1sYW1iZGEuZXUuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDIwMiIsImF1ZCI6Ik5EQmIwRkROYTk4VHNnZkx2MlE3S09BbHFCb0JuQW5NIiwiaWF0IjoxNjUwNTIzMjIyLCJleHAiOjE2NTA1MjY4MjJ9.PzAxAoSqyt2YrbCiQJjWvRF8_--XD-BJq8ddyJpA5MnYPKj-90f7VRbVPDtmIoEdruBrL5gMcTb5lRpGJ4Rc2ctS3mVS6hzQb8bV9BRjnLJZzk79Fw81y--tUJRhpJ5gydvtOv_W2qGL1Pz81_rlirdN_ZJzd-3alRIBwLkghdKvQmdinhfnBpoUj-lO3Ooqj2egCo1zqd5p3MXEncPNeW0Xw8jEZ89xJydKCPCvvHA0EPTP0ulV4mXfh2-gHRSv3ALAN8pHUwuvJuGSfPI1VH7-XWT0f3aEPcVptEPOoDiC7UtdtDxQzDtO8i81oxc4JV0zMmXlZm5klN7D3XZ-UQ',
        id:data.id,
        firstName:data.firstName,
        lastName:data.lastName,
        email:data.email, 
        companyId: data.companyId,
        adminTypeId:data.adminTypeId,
        roleId: data.rolesId,
        roleName:data?.roles.name,
        roleKey:data?.roles.key,
        companydetails:data?.company,
        token: token
      }
      await sequelize.connectionManager.close();
      return formatJSONResponse({
        statusCode: 200,
        message: `Anmeldung erfolgreich`,
        // message: `login successfully`,
        response,
      })
    
  } catch(error) {
    await sequelize.connectionManager.close();
    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
};

export const main = middyfy(loginUser);
