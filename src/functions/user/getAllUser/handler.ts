import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import {  users, company } from '../../../models/index';
import schema, { getAllUserSchema } from './schema';
import * as typings from '../../../shared/common';


const getAllUser:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{ 
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const data :typings.ANY   = await getAllUserSchema.validateAsync(event.body)
    const{
      limit,
      offset
    } = data
    const response : typings.ANY = await users.findAll({where:{ adminTypeId: 1 }, limit:limit,
      offset: offset * limit, attributes: {exclude: ['createdBy', 'updatedBy', 'deletedAt', 'deletedBy']},
      include:{
        as: 'company',
        model: company,
      }
    })

    if(!response){
      throw Error("User Not Exists ")
    }
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 200,
      message: `Erfolgreich erhalten`,
      // message: `Successfully Recieved`,
      response
    }); 
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    }); 
  }
}
export const main = middyfy(getAllUser);