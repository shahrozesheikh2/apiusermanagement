import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { company } from '../../../models/index';
import { sequelize } from '../../../config/database';
import schema, { companyUpdateSchema } from './schema';
import * as typings from '../../../shared/common';


const updateCompanyInfo: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message = 'Successfully updated'
    if(lang =='de')
    {
       message = `Erfolgreich geupdated`
    }
      const updateScheme: typings.ANY = await companyUpdateSchema.validateAsync(event.body) 
      const{
        id,
        companyDetail
      }= updateScheme

      // const {
      //   contactInfo
      // }= companyDetail

      const Data = await company.findOne({where:{ id }})

      if(!Data){
        await sequelize.connectionManager.close();

        return formatJSONResponse({
          statusCode: 403,
          message: `invalid ID`,
        })
      }

      // const{
      //   min:minNoOfEmp,
      //   max:maxNoOfEmp
      // } = noOfEmployees

      // return formatJSONResponse({
      //   statusCode: 200,
      //   message: message,
      //   companyDetail,
      //   contactInfo
      // });
        
      await company.update({ ...companyDetail , updatedAt: new Date }, {
        where:{
          id
        }
      })

      // for (let single of contactInformation){

      // }

      // await contactInfo.update({ ...companyDetail , updatedAt: new Date }, {
      //   where:{
      //     id
      //   }
      // })
      
      const response = await company.findOne({where:{ id }})
      
      await sequelize.connectionManager.close();
      
      return formatJSONResponse({
        statusCode: 200,
        message: message,
        response
      });
  } catch(error) {
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
};

export const main = middyfy(updateCompanyInfo);
