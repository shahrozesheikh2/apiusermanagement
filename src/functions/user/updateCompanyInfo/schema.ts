import * as Joi from 'joi'

export default {
    type: "object",
    properties: {
    }
} as const;
  
export const companyUpdateSchema = Joi.object({    
  id: Joi.number().integer().required(), 
  companyDetail:Joi.object({
    logo: Joi.object({
      awsKey: Joi.string().required(),
      awsUrl: Joi.string().uri().required()
    }),
    companyName: Joi.string(),
    website:Joi.string(),
    industryId: Joi.number().integer(),
    noOfEmployees: Joi.string(),
    taxId: Joi.number().integer(),
    allExpertise: Joi.array(),
    // .items({
    //   brandName:Joi.string(),
    //   productExpertise:Joi.string()
    // }),
    contactInfo:Joi.array().items({
      contactPerson: Joi.string().allow(null).allow(''),
      contactNumber:Joi.string().allow(null).allow(''),
      emailAddress:Joi.string().allow(null).allow(''),
      addtionalContactNo:Joi.string().allow(null).allow(''),
      additionalEmailAddress:Joi.string().allow(null).allow('')
    }), 
    address: Joi.string(),
    addressNotes: Joi.any(),
    countryId: Joi.number().integer(),
    cityId: Joi.number().integer(),
    stateId:Joi.number().integer(),
    postalCode: Joi.number().integer(),
   
    isBillingAddress: Joi.boolean(),
    billingAddress:  Joi.when('isBillingAddress',{is: true, then: Joi.string().required()}),
    billingAddressNotes: Joi.when('isBillingAddress',{is: true, then: Joi.string().required()}),
    billingAddressCountryId: Joi.when('isBillingAddress',{is: true, then: Joi.number().integer().required()}),
    billingAddressCityId:Joi.when('isBillingAddress',{is: true, then: Joi.number().integer().required()}),
    billingAddressStateId:Joi.when('isBillingAddress',{is: true, then: Joi.number().integer()}),
    billingAddressPostalCode: Joi.when('isBillingAddress',{is: true, then: Joi.number().integer().required()}),
    // sparePartTax:Joi.number().integer().required(),
    // serviceChargesTax:Joi.number().integer().required(),
    bankName:Joi.string(),
    accountTitle:Joi.string(),
    iban: Joi.string(),
    bic: Joi.string(),
    vat: Joi.number().integer(),
    payTermsDays:Joi.number().integer(),
    legalInformation:Joi.array()
  }).required(),
  
})

 
    