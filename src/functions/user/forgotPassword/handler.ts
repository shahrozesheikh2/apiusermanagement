import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import {  users } from '../../../models/index';
import { sequelize } from '../../../config/database';
import schema from './schema';
import { makeid } from '../../../shared/common/common';
import fetch from 'node-fetch'

const forgotPassword: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
  try{
    sequelize.connectionManager.initPools();
    var lang = event.headers['accept-language']
    if(lang =='en')
    {
      lang ='';
    }
    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const transaction = await sequelize.transaction()
    // const ses = new AWS.SES({ region: 'eu-central-1' }) 
    
    try{

      const data = event.body

      const {
        email
      }= data


      const userExist = await users.findOne({ where:  { email, deletedAt: null } }) 

      if(!userExist){
      await sequelize.connectionManager.close();

        return formatJSONResponse({
          message: `Email already exist`,
        })
      }

      const genratedLink = `${makeid(150)}`

      await users.update({genratedLink},{where:{email, id:userExist.id}})
      var params = {
        subject: 'FixFirst: Reset Your Password',
        body: `Hello ${userExist.firstName} ${userExist.lastName} To reset your password for FixFirst, please click the following link: 
         
        https://dev.app.fixfirst.io/auth/reset-password?id=${userExist.id}&genratedLink=${genratedLink}&rolesId=${userExist.rolesId}
         
        If you don't want to reset your password, you can ignore this message - someone probably typed in your username or email address by mistake.
         
        Thanks!
        Team FixFirst`,
        recipient: email
      }
      if(lang=='de')
      {
        params = {
          subject: 'FixFirst: Dein Passwort zurücksetzen',
          body:`Setze dein Passwort zurück
                Hallo ${userExist.firstName} ${userExist.lastName} 
                um dein Passwort für FixFirst zurückzusetzen, klicke bitte auf den folgenden Link: 
                https://dev.app.fixfirst.io/${lang}/auth/reset-password?id=${userExist.id}&genratedLink=${genratedLink}&rolesId=${userExist.rolesId}
                Wenn du dein Passwort nicht zurücksetzen möchtest, kannst du diese Nachricht ignorieren - wahrscheinlich hat jemand deinen Benutzernamen oder deine E-Mail-Adresse versehentlich eingegeben.
           
                Vielen Dank!
                Dein FixFirst-Team`,
          recipient: email
        }
      }
     
         


      
      const emailServiceResponse = await fetch(`${process.env.SEND_EMAIL_SES}/sendMail`, {
        method: 'POST',
        body: JSON.stringify(params),
        headers: { 'Content-Type': 'application/json' }
      })
      .then(res => res.json())
      .then(json => json)
      .catch(err => err);

      await transaction.commit()
      
      // await Axios.post(`${process.env.SEND_EMAIL_SES}/sendMail`, params);
      await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 200,
        message: `E-Mail erhalten`,
        // message: `recieved an email`,
        emailServiceResponse
      })
    } 
    catch (error: any) {
      if (transaction) {transaction.rollback(); }
      // throw error
      await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 403,
        message: error.message
       
      })
    }
  } 
  catch(error) {
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
};

export const main = middyfy(forgotPassword);