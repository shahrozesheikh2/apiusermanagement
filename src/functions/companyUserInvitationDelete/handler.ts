import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { users } from '../../models/index';
import { sequelize } from '../../config/database';
import schema, {companyUserDeleteScheme} from './schema';
import * as typings from '../../shared/common';
import customMiddleware from 'src/shared/common/middleware';


const companyUserInvitationDelete: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    const deleteScheme: typings.ANY = await companyUserDeleteScheme.validateAsync(event.body) 
    const{
      id
    } = deleteScheme

    const companyUserData = await users.findOne({where:{id}})

    if(!companyUserData){
      await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 404,
        message: `invalid ID`,
      });
    }
      
    const Response: any = await users.update({...deleteScheme ,deletedAt: new Date} ,{where: {id}})
      
      
    await sequelize.connectionManager.close();
      
      return formatJSONResponse({
        statusCode: 200,
        message: `Unternehmensbenutzer erfolgreich gelöscht`,
        // message: `delete successfully Company User`,
        Response
      });
    
  } catch(error) {
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
};

export const main = middyfy(companyUserInvitationDelete).use(customMiddleware());
