import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { makeid} from '../../shared/common/common'
import { roles, users, usersI } from '../../models/index';
import { sequelize } from '../../config/database';
import schema, {companyUserRegisterScheme} from './schema';
import * as typings from '../../shared/common';
// import Axios from 'axios'
import fetch from 'node-fetch'
import customMiddleware from 'src/shared/common/middleware';

const companyUserCreate: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message ='user created successfully'
    var errormessage = 'Email already exist '
    console.log(event.headers);
    if(lang =='en')
    {
      lang ='';
    }
    if(lang =='de')
    {
      message = 'Benutzerkonto erfolgreich erstellt'
      errormessage = 'E-Mail bereits vorhanden'
    }
      const registerScheme: typings.ANY = await companyUserRegisterScheme.validateAsync(event.body)
 
      const{
        companyId,
        companyUsers
      }= registerScheme
      const adminUser = await users.findOne({ where:{ companyId, adminTypeId:1 ,deletedAt:null }})
      
      let usersArray = []
      for(let user of companyUsers){
        const emailCheck = await users.findOne({ where:{ email: user.email ,deletedAt:null }})
        if(emailCheck) throw new Error(errormessage); 
        // Email Already exisited
        const genratedLink = `${makeid(150)}`

        usersArray.push({...user, companyId ,userStatusId:1, createdBy:adminUser.id, emailVerified:0, genratedLink })
      }
    
      const companyUserResponse: usersI[] = await users.bulkCreate(usersArray, { individualHooks: true })
      
      // const emailService = companyUserResponse.map(async item => {
      //   const params = {
      //     subject: 'hello from fixfirst',
      //     body: `Hello ${item.email} you have successfull register your account please click the link to complete the registration https://app.fixfirst.io/auth/sign-up/step-4?id=${item.id}&genratedLink=${item.genratedLink}&rolesId=${item.rolesId}&userStatusId=2`,
      //     recipient: item.email
      //   }
      //   // Axios.post(`${process.env.SEND_EMAIL_SES}/sendMail`, params);
      //   try {
      //     const res = await fetch(`${process.env.SEND_EMAIL_SES}/sendMail`, {
      //       method: 'POST',
      //       body: JSON.stringify(params),
      //       headers: { 'Content-Type': 'application/json' }
      //     });
      //     const json = await res.json();
      //     return json;
      //   } catch (err) {
      //     return err;
      //   }
          
      // })

  

      let emailServiceResponse = []
      for(let item of companyUserResponse){
        console.log(lang);
        let role = await roles.findOne({where:{id:item.rolesId}})
        var params = {
          subject: 'You’re Invited! FixFirst journey towards a circular future',
          body: `
          Hello ${item.firstName} ${item.lastName} invited you to join the organisation: FixFirst as ${role.name}, and we are excited to have you in our team. To get started click on the link below to dive into the team. 
          https://dev.app.fixfirst.io/auth/sign-up/step-4?id=${item.id}&genratedLink=${item.genratedLink}&rolesId=${item.rolesId}&userStatusId=2
          Thanks!
          Team FixFirst`,
          recipient: item.email
        }

         if(lang=='de')
         {
          params = {
            subject: 'Du bist eingeladen auf die Reise mit FixFirst in eine zirkuläre Zukunft!',
            body: `Du wurdest zu FixFirst eingeladen
            Hallo! ${item.firstName} ${item.lastName} hat dich eingeladen, der Organisation beizutreten: FixFirst als Servicetechniker. Wir freuen uns, dich bei unserem Team dabei zu haben. Um loszulegen, klicke einfach auf den untenstehenden Link um deinem Team beizutreten. 
            https://dev.app.fixfirst.io/${lang}/auth/sign-up/step-4?id=${item.id}&genratedLink=${item.genratedLink}&rolesId=${item.rolesId}&userStatusId=2
            
            Vielen Dank!
            Dein FixFirst-Team`,
            recipient: item.email
         }
      
        }

        const emailService = await fetch(`${process.env.SEND_EMAIL_SES}/sendMail`, {
          method: 'POST',
          body: JSON.stringify(params),
          headers: { 'Content-Type': 'application/json' }
        })
        .then(res => res.json())
        .then(json => json)
        .catch(err => err);

      emailServiceResponse.push({emailService})
      }
      
      // console.log(emailServiceResponse);
      


      

      // const emailServiceResponse = fetch(`${process.env.SEND_EMAIL_SES}/sendMail`, {
      //   method: 'POST',
      //   body: JSON.stringify(emailService),
      //   headers: { 'Content-Type': 'application/json' }
      // })
      //   .then(res => res.json())
      //   .then(json => json)
      //   .catch(err => err);
      await sequelize.connectionManager.close();
      
      return formatJSONResponse({
        statusCode: 200,
        message: message,
        // user created successfully
        companyUserResponse,
        emailServiceResponse
      });
    
  } 
  catch(error) {
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
}

export const main = middyfy(companyUserCreate).use(customMiddleware());
