import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;

export const companyUserRegisterScheme = Joi.object({ 
  companyId: Joi.number().integer().required(),
  userId:Joi.number().integer(),
  companyUsers: Joi.array().items({
    rolesId: Joi.number().integer().required(),
    firstName:Joi.string().required(),
    lastName:Joi.string().required(),
    email: Joi.string().email().required(),
  }).required(),  
})