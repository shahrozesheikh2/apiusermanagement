import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { city } from '../../models/index';
import schema, { getAllCitiesSchema } from './schema';
import * as typings from '../../shared/common';
// import translate from "translate";

const getAllCities:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  
  try{ 
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    // var lang = event.headers['Accept-Language'];

    const data :typings.ANY  = await getAllCitiesSchema.validateAsync(event.body)

    const{
      countryId,
      // limit,
      // offset
    } = data
    
    let response : typings.ANY = await city.findAll({
      where:{ countryId }, attributes: {exclude: ['createdBy', 'updatedBy', 'deletedAt', 'deletedBy', 'createdAt', 'updatedAt','cityId','lang','countryId']},
      // limit:limit,
      // offset: offset * limit
    })
    await sequelize.connectionManager.close();

    // console.log("lang",lang)
    // if (lang === 'de'){
    //   response = await translate(JSON.stringify(response), { to: "de" });
    //   console.log("response",response)
    //   throw new Error
    //   const abc = await JSON.parse(response)
    //   console.log("abc",abc)
    // }

    return formatJSONResponse({
      statusCode: 200,
      message: `Erfolgreich erhalten`,
      // message: `Successfully Recieved`,
      response
    }); 
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    }); 
  }
}
export const main = middyfy(getAllCities);