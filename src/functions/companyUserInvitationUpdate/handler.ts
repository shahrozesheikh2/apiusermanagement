import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { users } from '../../models/index';
import { sequelize } from '../../config/database';
import schema, { companyUserUpdateScheme } from './schema';
import * as typings from '../../shared/common';
import customMiddleware from 'src/shared/common/middleware';


const companyUserInvitationUpdate: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
  try{
    sequelize.connectionManager.initPools();
    
    // console.log("saddasdasdasdasd")
    // throw Error

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message = 'Updated successfully'
    if(lang =='de')
    {
       message = `Erfolgreich geupdated`
    }
    const updateScheme: typings.ANY = await companyUserUpdateScheme.validateAsync(event.body) 

    const{
      id,
      ...other
    } = updateScheme

    const companyUserData = await users.findOne({where:{id}})
   
    if(!companyUserData){
      throw Error ('invalid Id')
      // return formatJSONResponse({
      //   statusCode: 404,
      //   message: `invalid ID`,
      // });
    }

      
    const Response: any = await users.update({...other ,updatedAt: new Date} ,{where: {id}})
      
    await sequelize.connectionManager.close();
    return formatJSONResponse({
      statusCode: 200,
      message: message,
      // message: `updated successfully`,
      Response
    });
    
  } catch(error) {
    await sequelize.connectionManager.close();
    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
};

export const main = middyfy(companyUserInvitationUpdate).use(customMiddleware());
