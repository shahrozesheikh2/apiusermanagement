import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;

export const companyUserUpdateScheme = Joi.object({ 
    id: Joi.number().integer(),
    rolesId: Joi.number().integer(),
    firstName:Joi.string(),
    lastName:Joi.string(),
    email: Joi.string().email(),
    active:Joi.boolean()
})