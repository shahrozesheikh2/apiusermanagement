import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import schema from './schema';
import AWS from "aws-sdk";

const RetriveResource: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async () => {

    try{
              
        const s3 = new AWS.S3()

        AWS.config.update({
            accessKeyId: process.env.A_ACCESS_KEY_ID,
            secretAccessKey: process.env.A_SECRET_ACCESS_KEY
        })

        const listResult = []
        
        try {  
            const params = {
                Bucket: process.env.FILE_UPLOAD_BUCKET_NAME
            }

            await s3.listObjects(params, function(err, data) {
                if (err) {
                    return 'There was an error viewing your album: ' + err.message
                }else{
                    for(let i of data.Contents){
                        listResult.push({awsKey: i.Key})
                    }
                    // data.Contents.forEach(function(obj){
                    //     listResult.push({awsKey: obj.Key})
                    // })
                }}).promise()

            return formatJSONResponse({
                message: `Successfully retrived file to S3`,
                listResult
            })
            
        } 
        catch (error) {
            console.error(error);
            return formatJSONResponse({
                message: error.message
            })
        }
    } 
    catch(error) {
        console.error(error);
        return formatJSONResponse({
            message: error.message
        })
    }
  };
  
  export const main = middyfy(RetriveResource);