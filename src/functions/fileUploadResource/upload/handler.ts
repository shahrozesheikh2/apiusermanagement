// import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
// import schema from './schema';
import AWS from "aws-sdk";

const uploadResource = async (event) => {
    try{

        AWS.config.update({
            accessKeyId: process.env.A_ACCESS_KEY_ID,
            secretAccessKey: process.env.A_SECRET_ACCESS_KEY
        })
        
        const s3 = new AWS.S3()
                
        const parsedBody = JSON.parse(JSON.stringify(event.body))
        // if(parsedBody.file.size > 26214400) throw new Error("File size too big. Limit 4mb")
        const base64File = parsedBody.file
        const decodedFile = Buffer.from(base64File.replace(/^data:image\/\w+;base64,/, ""), "base64")

        const randomID = parseInt(`${Math.random() * 10000000}`)
                

        const params = {
            Bucket: process.env.FILE_UPLOAD_BUCKET_NAME,
            Key: `${randomID}`,
            Body: decodedFile,
            ContentType: `${"image/jpeg" || "image/png"}`,
        }

        const uploadResult = await s3.upload(params).promise();

        if(!uploadResult){
            return formatJSONResponse({
                message: `error uploading in s3`
            })
        }

        return formatJSONResponse({
            message: `Bild erfolgreich hochgeladen`,
            // message: `Image successfully uploded`,
            awsKey: uploadResult.Key,
            awsUrl: uploadResult.Location
        })      
    } 
    catch(error) {
        console.error(error);
        return formatJSONResponse({
            message: error.message
        })
    }
  }
  
  export const main = middyfy(uploadResource);