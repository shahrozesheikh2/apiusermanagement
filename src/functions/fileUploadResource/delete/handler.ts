import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import * as typings from '../../../shared/common';
import schema, { deleteResourcesSchema } from './schema';
import AWS from "aws-sdk";

const deleteResource: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {

    try{
        
        const deleteImage: typings.ANY =  await deleteResourcesSchema.validateAsync(event.body)

        const{
            awsKeys
        }= deleteImage

        const deleteArray =[]

        AWS.config.update({
            accessKeyId: process.env.A_ACCESS_KEY_ID,
            secretAccessKey: process.env.A_SECRET_ACCESS_KEY
        })
        
        const s3 = new AWS.S3()
        
        try {
            
            for(const key of awsKeys){

                const params = {
                    Bucket: process.env.FILE_UPLOAD_BUCKET_NAME,
                    Key: key
                }

                const s3PromiseDelete=new Promise((resolve,reject)=>{
                    s3.deleteObject(params, function(err, data) {
                        if (err) {
                            reject(err);
                        }
                        resolve(data);
                    });
                })

                deleteArray.push(s3PromiseDelete)
            }
            
            await Promise.all(deleteArray);
            

            return formatJSONResponse({
                message: `Successfully deleted file to S3`
            })
            
        } 
        catch (error) {
            console.error(error);
            return formatJSONResponse({
                message: error.message
            })
        }
    } 
    catch(error) {
        console.error(error);
        return formatJSONResponse({
            message: error.message
        })
    }
  };
  
  export const main = middyfy(deleteResource);