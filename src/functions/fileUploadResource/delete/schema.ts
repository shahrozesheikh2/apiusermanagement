import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;
  
export const deleteResourcesSchema =  Joi.object({
    awsKeys : Joi.array().items(Joi.string().required()).required()
})
    