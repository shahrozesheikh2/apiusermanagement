import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { users, usersI, company, companyI, partners, partnersI } from '../../../models/index';
import { sequelize } from '../../../config/database';


const acceptInvitation = async (event) => {
  try {
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    try {

      const params = event.queryStringParameters

      const findToken = await partners.findOne({ where: { genratedLink: params.genratedLink } })

      if (!findToken) {
        await sequelize.connectionManager.close();

        return formatJSONResponse({
          message: `entered wrong token`,
        })
      }

      await partners.update({ accepted: true, updatedAt: new Date }, {
        where: { partnershipId: params.partnershipId }
      })

      await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 200,
        message: `Partnership Created`
      })
    }

    catch (error: any) {
      await sequelize.connectionManager.close();
      return formatJSONResponse({
        statusCode: 403,
        message: error.message
      })
    }
  }
  catch (error) {
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
};

export const main = middyfy(acceptInvitation);