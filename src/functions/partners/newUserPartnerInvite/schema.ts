import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;
  
export const newUserPartnerInvitationsSchema = Joi.object({   
  email:Joi.string().email().required(),
  companyId:Joi.number().integer().required(),
  userId: Joi.number().integer().required()
})