import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import {partners} from '../../../models/index';
import { sequelize } from '../../../config/database';
import schema, { newUserPartnerInvitationsSchema } from './schema';
import * as typings from '../../../shared/common';
import fetch from 'node-fetch'


const newUserPartnerInvitations: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
  try {
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`

    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    const transaction = await sequelize.transaction()

    try {

      const newUserPartnerInvitationsScheme: typings.ANY = await newUserPartnerInvitationsSchema.validateAsync(event.body)

      const {
        email,
        companyId,
        userId
      } = newUserPartnerInvitationsScheme

      let checkUserInvites = await partners.findAll({
        where: {
          invitedEmails: email
        }
      })

      if (checkUserInvites) {

        checkUserInvites = JSON.parse(JSON.stringify(checkUserInvites))

        checkUserInvites.forEach(async function (element) {
          let c: any = element.partnershipId
          let b: any = element.genratedLink

          var params = {
            subject: 'FixFirst Partnership Invitation',
            body: `Hello,
                 
            Your have been invited to join a partnership at FixFirst.
            
            https://dev.fixfirst.io/dev/main/partners/accept-partnership?partnershipId=${c}&genratedLink=${b}&userId
            
            ------------------------------------------------------------------------------------. 
            
            Thanks! 
            Team FixFirst`,
            recipient: email
          }

          const emailServiceResponse = await fetch(`${process.env.SEND_EMAIL_SES}/sendMail`, {
            method: 'POST',
            body: JSON.stringify(params),
            headers: { 'Content-Type': 'application/json' }
          })
            .then(res => res.json())
            .then(json => json)
            .catch(err => err);

          await partners.update({ accepted: false, updatedAt: new Date, companyId2: companyId, userId2: userId, invitedEmails: null }, {
            where: { partnershipId: c }
          })

          await transaction.commit()
          await sequelize.connectionManager.close();
        })

        return formatJSONResponse({
          statusCode: 200
        })
      }


    }
    catch (error: any) {
      if (transaction) { transaction.rollback(); }
      await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 403,
        message: error.message

      })
    }

  }
  catch (error) {
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
};

export const main = middyfy(newUserPartnerInvitations);