import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { users, partners, company } from '../../../models/index';
import schema, { getPartnerListingSchema } from './schema'
const { Op } = require("sequelize");
import * as typings from '../../../shared/common';


const getPartnerListing:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()' if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const requestData :typings.ANY   = await getPartnerListingSchema.validateAsync(event.body)
    
    const{
      companyId
    } = requestData

    console.log(requestData.companyId);

    let  whereClause: { [key: string]: typings.ANY }  = {}

    // if (companyId?.company2) {
    //   whereClause.bookingTypeIds = {  [Op.ne]: .bookingTypeIds }
    // }


;

    const response : typings.ANY = await partners.findAll({
      where: {
        companyId1: companyId,
        deletedAt: null, 
        accepted: 1
      }, 
      attributes: {exclude: [
       'industryId', 'genratedLink', 'createdAt','updatedAt','deletedAt','createdBy','updatedBy','deletedBy'
      ]},
      include:[{
        model: company,
        as: 'company2',
        attributes: [  'companyName', 'address'],
      },
      {
        model: users,
        as: 'user2',
        attributes: [ 'firstName', 'lastName', 'mobileNumber']
      }]
    })
    const response2 : typings.ANY = await partners.findAll({
      where: {
        companyId2: companyId,
        deletedAt: null, 
        accepted: 1
      }, 
      attributes: {exclude: [
        'industryId', 'genratedLink', 'createdAt','updatedAt','deletedAt','createdBy','updatedBy','deletedBy'
      ]},
      include:[{
        model: company,
        as: 'company1',
        attributes: [  'companyName', 'address'],
      },
      {
        model: users,
        as: 'user2',
        attributes: [ 'firstName', 'lastName', 'mobileNumber']
      }]
    })
    
    const Response : typings.ANY = response.concat(response2);

    // const response = sequelize.query('SELECT companyName FROM company WHERE Id in (SELECT companyId1 FROM partners WHERE id= :id',
    // {
    //   replacements: {id:companyId},
    //   type: QueryTypes.SELECT
    // })
    
    // if(!response.length){
    // await sequelize.connectionManager.close();

    //   return formatJSONResponse({
    //     statusCode: 403,
    //     message: `Can't Find Partners!`
    //   });
    // }

    await sequelize.connectionManager.close();

    console.log(Response.company2)

    return formatJSONResponse({
      statusCode: 200,
      message: `Partners found`,
      // message: `Successfully Recieved`,
      Response
    });

  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(getPartnerListing);