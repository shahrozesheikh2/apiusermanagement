import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { users, roles, partners, company } from '../../../models/index';
import schema, { getPartnerByIdSchema } from './schema';
import * as typings from '../../../shared/common';


const getPartnerById:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()' if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const requestData :typings.ANY   = await getPartnerByIdSchema.validateAsync(event.body)
    
    const{
      companyId,
      partnerId
    } = requestData

    const response : typings.ANY = await partners.findAll({
      where: { 
        deletedAt: null,
        companyId1: companyId,
        companyId2: partnerId,
        accepted: 1
      }, 
      attributes: {exclude: [
        'userId1', 'companyId1', 'industryId', 'genratedLink', 'createdAt','updatedAt','deletedAt','createdBy','updatedBy','deletedBy', 'accepted'
      ]},
      include:[{
        model: company,
        as: 'company2',
        attributes: [ 'id', 'companyName']
      },
      {
        model: users,
        as: 'user2',
        attributes: [ 'firstName', 'lastName']
      }]
    })


    
    if(!response.length){
    await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 403,
        message: `Partner doesn't exist with this ID`
      });
    }

    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 200,
      message: `Partner by ID found`,
      // message: `Successfully Recieved`,
      response
    });

  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(getPartnerById);