import * as Joi from 'joi'

export default {
    properties: {
    }
} as const;


export const getPartnerByIdSchema =  Joi.object({
    partnerId: Joi.number().integer().required(),
    companyId: Joi.number().integer().required(),
    // filters: Joi.object({
    //     preferedLanguage: Joi.number().integer(),
    // }),
    // preferedLanguage: Joi.array().items(Joi.number().integer())
})  