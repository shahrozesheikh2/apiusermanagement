import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;
  
export const invitePartnerSchema = Joi.object({   
  email:Joi.string().email().required()
})