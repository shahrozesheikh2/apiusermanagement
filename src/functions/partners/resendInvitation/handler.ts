import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { users } from '../../../models/index';
import { sequelize } from '../../../config/database';
import schema from './schema';
import { makeid } from '../../../shared/common/common';
import fetch from 'node-fetch'

const resendInvitation: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
  try {
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    const transaction = await sequelize.transaction()
    // const ses = new AWS.SES({ region: 'eu-central-1' }) 

    try {

      const data = event.body

      const {
        email
      } = data


      const userExist = await users.findOne({ where: { email, deletedAt: null } })

      if (!userExist) {
        await sequelize.connectionManager.close();

        return formatJSONResponse({
          message: `Email already exist`,
        })
      }

      const genratedLink = `${makeid(150)}`

      await users.update({ genratedLink }, { where: { email } })
      var params = {
        subject: 'Fixfirst Partnership Invite',
        body: `Hello ${userExist.firstName} ${userExist.lastName},
        You are invited to form a partnership at FixFirst
        https://dev.app.fixfirst.io/auth/sign-up/step-4?id=${userExist.id}&genratedLink=$
        If you didn’t request this email, there’s nothing to worry about — you can safely ignore it. 
        
        Thanks! 
        Team FixFirst`,
        recipient: email
      }

      //{genratedLink}&rolesId=${userExist.rolesId}
        




      const emailServiceResponse = await fetch(`${process.env.SEND_EMAIL_SES}/sendMail`, {
        method: 'POST',
        body: JSON.stringify(params),
        headers: { 'Content-Type': 'application/json' }
      })
        .then(res => res.json())
        .then(json => json)
        .catch(err => err);

      await transaction.commit()

      // await Axios.post(`${process.env.SEND_EMAIL_SES}/sendMail`, params);
      await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 200,
        // message: `Email has been resent`,
        message: `E-Mail wurde erneut gesendet`,
        emailServiceResponse
      })
    }
    catch (error: any) {
      if (transaction) { transaction.rollback(); }
      // throw error
      await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 403,
        message: error.message

      })
    }
  }
  catch (error) {
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
};

export const main = middyfy(resendInvitation);