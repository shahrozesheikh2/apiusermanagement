import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { users, partners } from '../../../models/index';
import { sequelize } from '../../../config/database';
import schema, { invitePartnerSchema } from './schema';
import * as typings from '../../../shared/common';
import { makeid } from '../../../shared/common/common';
import fetch from 'node-fetch'

const invitePartner: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
  try {

    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`

    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()

    const transaction = await sequelize.transaction()
    var message = 'Partnership Offer Email Sent Successfully'

    try {
      const invitePartnerScheme: typings.ANY = await invitePartnerSchema.validateAsync(event.body)

      const {
        email,
        companyId1,
        userId1,
        companyName
      } = invitePartnerScheme

      const invitedEmails = email;

      // console.log("email......qw:", email, "companyId2.....qw:", companyId2, "userId2.......qw", userId2)


      const userExists = await users.findOne({ where: { email, deletedAt: null } })


      const genratedLink = `${makeid(150)}`


      if (userExists) {

        const userId2 = userExists.id
        const companyId2 = userExists.companyId

        const obj: any = {
          userId1,
          userId2,
          companyId1,
          companyId2,
          accepted: 0,
          genratedLink
        }

        console.log("objjjjjjjjj: ", obj)

        const firstName = userExists.firstName;
        const lastName = userExists.lastName;



        const partnerResponse = await partners.create(obj, { transaction })

        const partnershipId = partnerResponse.partnershipId;

        var params = {
          subject: 'FixFirst Partnership Invitation',
          body: `Hello ${firstName} ${lastName},
               
          Your have been invited to form a partnership at FixFirst by ${companyName}

          Click the link below to accept the partnership with ${companyName}
          
          https://dev.fixfirst.io/dev/main/partners/accept-partnership?partnershipId=${partnershipId}&genratedLink=${genratedLink}
          
          ------------------------------------------------------------------------------------. 
          
          Thanks! 
          Team FixFirst`,
          recipient: email
        }

        const emailServiceResponse = await fetch(`${process.env.SEND_EMAIL_SES}/sendMail`, {
          method: 'POST',
          body: JSON.stringify(params),
          headers: { 'Content-Type': 'application/json' }
        })
          .then(res => res.json())
          .then(json => json)
          .catch(err => err);

        await transaction.commit()
        await sequelize.connectionManager.close();

        return formatJSONResponse({
          statusCode: 200,
          message: message,
          // message: `Partnership Offer Sent Successfully`
          emailServiceResponse,
          partnerResponse
        })
      } else {
    
        const invitedEmailsDate = new Date;
        const obj: any = {
          invitedEmails,
          userId1,
          companyId1,
          genratedLink,
          invitedEmailsDate

        }

        const nonUserResponse = await partners.create(obj, { transaction })

        var params = {
          subject: 'FixFirst Partnership Invitation',
          body: `Hello,
               
          Your have been invited to sign-up at FixFirst by ${companyName} and form a partnership.
          
          http://localhost:4200/auth/sign-in
          
          After you sign up you will recieve a new email from FixFirst in which there will be a link to accept partnership from
          ------------------------------------------------------------------------------------. 
          
          Thanks! 
          Team FixFirst`,
          recipient: email
        }

        const emailServiceResponse = await fetch(`${process.env.SEND_EMAIL_SES}/sendMail`, {
          method: 'POST',
          body: JSON.stringify(params),
          headers: { 'Content-Type': 'application/json' }
        })
          .then(res => res.json())
          .then(json => json)
          .catch(err => err);

        await transaction.commit()
        await sequelize.connectionManager.close();

        return formatJSONResponse({
          statusCode: 200,
          message: 'They have been invited to join FixFirst. After they sign up, they can become partners with you',
          // message: `Partnership Offer Sent Successfully`
          emailServiceResponse,
          nonUserResponse
        })


      }

    }
    catch (error: any) {
      if (transaction) { transaction.rollback(); }
      await sequelize.connectionManager.close();
      return formatJSONResponse({
        statusCode: 403,
        message: error.message

      })
    }
  }
  catch (error) {
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
};

export const main = middyfy(invitePartner);

//response.id