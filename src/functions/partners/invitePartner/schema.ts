import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;
  
export const invitePartnerSchema = Joi.object({   
  email:Joi.string().email().required(),
  companyId1:Joi.number().integer().required(),
  userId1: Joi.number().integer().required(),
  companyName: Joi.string()
})