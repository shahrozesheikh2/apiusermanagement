import * as Joi from 'joi'

export default {
    properties: {
    }
} as const;


export const deletePartnerSchema =  Joi.object({
    partnerId: Joi.number().integer().required(),
    companyId: Joi.number().integer().required(),
    partnershipId: Joi.number().integer().required(),
})