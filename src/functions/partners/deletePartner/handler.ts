import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { users, roles, partners, company } from '../../../models/index';
import schema, { deletePartnerSchema } from './schema';
import * as typings from '../../../shared/common';
import { Op } from 'sequelize';
 

const deletePartner: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
  try {
    sequelize.connectionManager.initPools();

    // restore `getConnection()' if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const requestData: typings.ANY = await deletePartnerSchema.validateAsync(event.body)

    const {
      companyId,
      partnerId,
      partnershipId
    } = requestData

    const findPartner: typings.ANY = await partners.findOne({
      where: {
        deletedAt: null,
        companyId1: companyId,
        companyId2: partnerId,
        accepted: 1
      }
    })

    if (!findPartner){
      throw new Error('Invalid Partner-ID or Partnership-ID or company-ID')
    }

    const data = await partners.update({ deletedAt: new Date }, { where: { partnershipId } })



    // if (!findPartner.length) {
    //   await sequelize.connectionManager.close();

    //   return formatJSONResponse({
    //     statusCode: 403,
    //     message: `Partner doesn't exist with this ID`
    //   });
    // }

    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 200,
      message: `Partner by ID found and Deleted`,
      data
    });

  }
  catch (error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(deletePartner);