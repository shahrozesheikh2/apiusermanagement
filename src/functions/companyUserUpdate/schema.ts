import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;

export const companyUserUpdateSchema = Joi.object({ 
    id:Joi.number().integer(),
    resource: Joi.object({
      awsKey: Joi.string(),
      awsUrl: Joi.string().uri()
    }),
    firstName:Joi.string(),
    lastName:Joi.string(),
    timeZone:Joi.string(),
    password:Joi.string().min(8),
    phoneNumberExtention: Joi.string(),
    phoneNumber: Joi.string(),
    isWhatsAppMblNo: Joi.boolean(),
    additionNumberExtension: Joi.string(),
    additionNumber: Joi.string(),
    isWhatsAppAddNo: Joi.boolean(),
    organization: Joi.string(),
    address: Joi.string(),
    addressNumber: Joi.number().integer(),
    country: Joi.number().integer(),
    postalCode: Joi.number().integer(),
    city: Joi.number().integer(),
    languages:Joi.array(),
    certification: Joi.array(),
    brandsExpertise: Joi.array(),
    productsExpertise: Joi.array(),
    physicalResilience: Joi.boolean(),
    personalProfile: Joi.string(),
})