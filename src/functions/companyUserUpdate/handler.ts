import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { users } from '../../models/index';
import { sequelize } from '../../config/database';
import schema, {companyUserUpdateSchema} from './schema';
import * as typings from '../../shared/common';
import bcrypt from "bcryptjs";

const companyUserUpdate: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    var lang = event.headers['accept-language']
    var message = 'Successfully updated'
    if(lang =='de')
    {
       message = `Erfolgreich geupdated`
    }
      const updateScheme: typings.ANY = await companyUserUpdateSchema.validateAsync(event.body)
       
      const{
        id
      }= updateScheme
      const Data = await users.findOne({where:{id}})
      if(!Data){
      await sequelize.connectionManager.close();

        return formatJSONResponse({
          statusCode: 404,
          message: `invalid ID`,
        });
      }
      
      // const hash = await bcrypt.hash(updateScheme.password, 10);
      // updateScheme.password = hash;
      if(updateScheme.password ){
        const passwordHash =await bcrypt.hash(updateScheme.password, 10);
        updateScheme.password = passwordHash
      }
      await users.update({ ...updateScheme, updatedAt: new Date, emailVerified: true }, {
        where:{
          id: id
        }
      })
      await sequelize.connectionManager.close();
      
      return formatJSONResponse({
        statusCode: 200,
        message: message,
        // updated successfully
      })
    
  } 
  catch(error) {
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    })
  }
};

export const main = middyfy(companyUserUpdate);
