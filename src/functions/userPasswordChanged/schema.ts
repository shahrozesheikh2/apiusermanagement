import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;

export const userPasswordChangedSchema = Joi.object({
    id: Joi.number().integer().required(),
    previousPassword:Joi.string().required(),
    newPassword:Joi.string().required().min(8),
    // isCompanyUser:Joi.boolean().required()
})