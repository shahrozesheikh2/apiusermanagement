import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { users } from '../../models/index';
import { sequelize } from '../../config/database';
import schema, {userPasswordChangedSchema} from './schema';
import * as typings from '../../shared/common';
import bcrypt from "bcryptjs";

const userPasswordChanged: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
   
    
    try{
      sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
      sequelize.authenticate()
      var lang = event.headers['accept-language']
      var message = 'Successfully updated'
      var errormessage = 'Previous Password Invalid'
      if(lang =='de')
      {
         message = `Erfolgreich geupdated`
         errormessage ='Vorheriges Passwort ungültig'
      }
      const passwordBody: typings.ANY = await userPasswordChangedSchema.validateAsync(event.body)
      const{
        id,
        previousPassword,
        newPassword,
        // isCompanyUser
      }= passwordBody


      const userExist = await users.findOne({ where:{ id, deletedAt: null }})

        if(!userExist){
          throw new Error('Id does not exist');
        }

      const passwordHashing = bcrypt.compareSync(previousPassword, userExist.password)

        if(!passwordHashing){
        await sequelize.connectionManager.close();

          return formatJSONResponse({
            statusCode: 403,
            message: errormessage,
            // message: `Previous Password Invalid`,
          });
        }
      const hash = await bcrypt.hash(newPassword, 10);
        await users.update({ password:hash },{where:{ id }})

      // if(isCompanyUser == true) {

      //   const CompanyUserExist = await users.findOne({ where:{ id, deletedAt: null, companyId: null }})

      //   if(!CompanyUserExist){
      //     throw new Error('Id does not exist');
      //   }

      //   const passwordHashing= bcrypt.compareSync(previousPassword, CompanyUserExist.password)

      //   if(!passwordHashing){
      //     return formatJSONResponse({
      //       statusCode: 403,
      //       message: `Previous Password Invalid`,
      //     });
      //   }
      //   const hash = await bcrypt.hash(newPassword, 10);
      //    await users.update({ password:hash },{ where:{ id } })

      // }else{

      //   const userExist = await users.findOne({ where:{ id, deletedAt: null }})

      //   if(!userExist){
      //     throw new Error('Id does not exist');
      //   }

      //   const passwordHashing = bcrypt.compareSync(previousPassword, userExist.password)

      //   if(!passwordHashing){
      //     return formatJSONResponse({
      //       statusCode: 403,
      //       message: `Previous Password Invalid`,
      //     });
      //   }
      //   const hash = await bcrypt.hash(newPassword, 10);
      //    await users.update({password:hash},{where:{id}})
      // }
      await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 200,
        message: message,

      });
    } 
    catch (error: any) {
      await sequelize.connectionManager.close();

      return formatJSONResponse({
        statusCode: 403,
        message: error.message
      })
    }
 
}

export const main = middyfy(userPasswordChanged);
