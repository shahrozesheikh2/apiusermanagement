import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { roles } from '../../models/index';
import schema from './schema';
import * as typings from '../../shared/common';
import { Op } from 'sequelize';


const getAllRoles:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    var lang = event.headers['accept-language']

    let message =`Successfully Recieved`

    if(lang =='de')
    {
      message =  `Erfolgreich erhalten`
    }


    const response : typings.ANY = await roles.findAll({ where:{deletedAt:null,lang:lang , key:{[Op.ne]:'super_admin'} },
      attributes: {exclude: ['createdBy', 'updatedBy', 'deletedAt', 'deletedBy', 'createdAt', 'updatedAt']}
    })
    await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
      message,
      response
     });
  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(getAllRoles);