import * as Joi from 'joi'

export default {
    properties: {
    }
} as const;


export const getAllCompanyUserSchema =  Joi.object({
    companyId: Joi.number().integer().required(),
    userId:Joi.number().integer(),
    // filters: Joi.object({
    //     preferedLanguage: Joi.number().integer(),
    // }),
    preferedLanguage: Joi.array().items(Joi.number().integer())
})