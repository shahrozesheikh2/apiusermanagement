import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { users, roles } from '../../models/index';
import schema, { getAllCompanyUserSchema } from './schema';
import * as typings from '../../shared/common';
import { Op } from 'sequelize';


const getAllTechnicianByCompanyId:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
  try{
    sequelize.connectionManager.initPools();

    // restore `getConnection()` if it has been overwritten by `close()`
    if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
      delete sequelize.connectionManager.getConnection;
    }
    sequelize.authenticate()
    const data :typings.ANY   = await getAllCompanyUserSchema.validateAsync(event.body)
    const{
      companyId,
      userId,
      // filters,
      preferedLanguage
    } = data

    let  clause: { [key: string]: typings.ANY }  = {}

    // if(filters?.preferedLanguage){
    //   clause.languages = filters.preferedLanguage
    // }

    if(preferedLanguage && preferedLanguage.length){
      clause.languages = { [Op.in]: preferedLanguage}
    }
    if(userId){
      clause.createdBy =  userId;
    }


    const response : typings.ANY = await users.findAll({where:{
        companyId, ...clause, deletedAt: null, adminTypeId: null , emailVerified:true
        // languages: {
        //   [Op.in]: preferedLanguage
        // } 
      }, 
      attributes: [
        'id', 'companyId', 'firstName', 
        'lastName', 'email', 'languages', 'createdAt', 'updatedAt'
      ],
      include:{
        as: 'roles',
        model: roles,
        where: {key: 'technician'},
        attributes:['name']
      }
    })
    
    // if(!response.length){
    //   return formatJSONResponse({
    //     statusCode: 403,
    //     message: `User Not Exists Against Current Id`,
    //   });
    // }

    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 200,
      message: `Erfolgreich erhalten`,
      // message: `Successfully Recieved`,
      response
    });

  } 
  catch(error) {
    console.error(error);
    await sequelize.connectionManager.close();

    return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
  }
}
export const main = middyfy(getAllTechnicianByCompanyId);