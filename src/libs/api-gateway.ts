import type { APIGatewayProxyEvent, APIGatewayProxyResult, Handler } from "aws-lambda"
import type { FromSchema } from "json-schema-to-ts";

type ValidatedAPIGatewayProxyEvent<S> = Omit<APIGatewayProxyEvent, 'body'> & { body: FromSchema<S> }
export type ValidatedEventAPIGatewayProxyEvent<S> = Handler<ValidatedAPIGatewayProxyEvent<S>, APIGatewayProxyResult>

export const formatJSONResponse = (response: Record<string, unknown>) => {
  return {
    body: JSON.stringify(response),
    isBase64Encoded: false,
    headers: {
      "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
      "Access-Control-Allow-Credentials" : true, // Required for cookies, authorization
      "Access-Control-Allow-Methods":"GET, POST, OPTIONS, PUT, PATCH, DELETE", // Request methods you wish to allow
      "Access-Control-Allow-Headers": "X-Requested-With,content-type" //Request headers you wish to allow
    }
  }
}
